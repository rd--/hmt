module Music.Theory.Time where

-- | Time point.
type Time = Rational

-- | Time interval.
type Duration = Rational

-- | Tempo is rational.
type Tempo = Rational

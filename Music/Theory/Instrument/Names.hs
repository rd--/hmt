module Music.Theory.Instrument.Names where

import Data.List.Split {- split -}

-- | (family,abbreviations,names,transpositions)
instrument_db' :: [(String, String, String, String)]
instrument_db' =
  [ ("br", "b.tbn", "bass trombone", "")
  , ("br", "b.tuba", "bass tuba", "")
  , ("br", "euph", "euphonium", "")
  , ("br", "hn", "french horn", "F")
  , ("br", "tbn;trm", "trombone", "")
  , ("br", "tb;tba", "tuba", "")
  , ("br", "tpt", "trumpet", "B♭")
  , ("br", "t.tbn", "tenor trombone", "")
  , ("br", "crt", "cornet", "")
  , ("br", "fgh;flhn", "flugel horn", "")
  , ("br", "p.tpt", "piccolo trumpet", "")
  , ("el", "cd", "compact disc", "")
  , ("el", "el;elec", "electronics", "")
  , ("el", "tp", "tape", "")
  , ("el", "om", "ondes martenot", "")
  , ("kb", "h;hrm", "harmonium", "")
  , ("kb", "e.pf", "electric piano", "")
  , ("kb", "p;pf;pno", "piano;pianoforte", "")
  , ("kb", "o;or;org", "organ", "")
  , ("kb", "kb;kbd", "keyboard", "")
  , ("kb", "cel", "celeste", "")
  , ("kb", "clvd", "clavichord", "")
  , ("kb", "hpd;hpcd", "harpsichord", "")
  , ("kb", "syn", "synthesiser", "")
  , ("pc", "bd", "bass drum", "")
  , ("pc", "btl", "bottle", "")
  , ("pc", "cast", "castanets", "")
  , ("pc", "cbell", "cow-bell", "")
  , ("pc", "bell", "bell", "chimes")
  , ("pc", "clv", "clave", "")
  , ("pc", "crot", "crotales", "")
  , ("pc", "cym", "cymbals", "")
  , ("pc", "dm", "drum", "")
  , ("pc", "gl;glsp", "glockenspiel", "")
  , ("pc", "mcas", "maracas", "")
  , ("pc", "met.bl", "metal block", "")
  , ("pc", "mr;mar", "marimba", "")
  , ("pc", "sd", "side drum", "")
  , ("pc", "sn.dm", "snare drum", "")
  , ("pc", "sus.cym", "suspended cymbal", "")
  , ("pc", "tamb", "tambourine", "")
  , ("pc", "tam", "tam tam", "")
  , ("pc", "t.bells", "tubular bells", "")
  , ("pc", "td", "tenor drum", "")
  , ("pc", "tri;tgl", "triangle", "")
  , ("pc", "tm;timp", "timpani", "")
  , ("pc", "tpl.bl", "temple blocks", "")
  , ("pc", "vb;vib", "vibraphone", "")
  , ("pc", "wdbl", "wood block", "")
  , ("pc", "xyl", "xylophone", "")
  , ("str", "va;vla", "viola", "")
  , ("str", "vc;vlc", "cello;violoncello", "")
  , ("str", "vn;vln", "violin", "")
  , ("str", "cb", "contrabass", "")
  , ("str", "db", "double bass", "")
  , ("str", "vda", "viola d'amore", "")
  , ("str", "b.gtr", "bass guitar", "")
  , ("str", "e.gtr", "electric guitar", "")
  , ("str", "gtr", "guitar", "")
  , ("str", "", "lute", "")
  , ("str", "zith", "zither", "")
  , ("str", "hp", "harp", "")
  , ("str", "dulc", "dulcimer", "")
  , ("str", "mand", "mandolin", "")
  , ("vc", "a;alt", "alto", "")
  , ("vc", "b;bass", "bass", "")
  , ("vc", "mz;mez", "mezzo-soprano", "")
  , ("vc", "n;nar", "narrator", "")
  , ("vc", "s;sop", "soprano", "")
  , ("vc", "t;tn", "tenor", "")
  , ("vc", "v;vc;voc", "voice", "")
  , ("vc", "ch", "chorus", "")
  , ("vc", "ctral", "contralto", "")
  , ("vc", "ctrbs", "contrabass", "")
  , ("vc", "bar", "baritone", "")
  , ("vc", "b.bar", "bass baritone", "")
  , ("ww", "b.cl", "bass clarinet", "")
  , ("ww", "cb.cl", "contrabass clarinet", "")
  , ("ww", "c;cl", "clarinet", "B♭")
  , ("ww", "a.fl", "alto flute", "G")
  , ("ww", "b.fl", "bass flute", "C")
  , ("ww", "bn;bsn", "bassoon", "")
  , ("ww", "f;fl", "flute", "")
  , ("ww", "hb;htb", "hautbois", "")
  , ("ww", "o;ob", "oboe", "")
  , ("ww", "p;picc", "piccolo", "")
  , ("ww", "ca", "cor anglais", "")
  , ("ww", "c.bn", "contrabassoon", "")
  , ("ww", "a.sax", "alto saxophone", "E♭")
  , ("ww", "b.sax", "baritone saxophone", "E♭")
  , ("ww", "b.ob", "bass oboe", "")
  , ("ww", "cfg", "contrafagotto", "")
  , ("ww", "eh;en.hn", "english horn", "")
  , ("ww", "fg", "fagotto", "")
  , ("ww", "rec", "recorder", "")
  , ("ww", "sax", "saxophone", "")
  , ("ww", "s.sax", "soprano saxophone", "B♭")
  , ("ww", "t.sax", "tenor saxophone", "B♭")
  , ("ww", "oca", "ocarina", "")
  ]

-- | (family,[abbreviations],[names],[transpositions])
instrument_db :: [(String, [String], [String], [String])]
instrument_db =
  let sep = splitOn ";"
      f (fm, ab, nm, tr) = (fm, sep ab, sep nm, sep tr)
  in map f instrument_db'

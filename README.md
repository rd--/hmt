hmt - haskell music theory
--------------------------

[haskell](http://haskell.org/) music theory

requires:

- [hmt-base](http://rohandrape.net/?t=hmt-base)

related:

- [hmt-diagrams](http://rohandrape.net/?t=hmt-diagrams)
- [hmt-texts](http://rohandrape.net/?t=hmt-texts)

## cli

[csv-midi](http://rohandrape.net/?t=hmt&e=md/csv-midi.md),
[db](http://rohandrape.net/?t=hmt&e=md/db.md),
[gl](http://rohandrape.net/?t=hmt&e=md/gl.md),
[gr-planar](http://rohandrape.net/?t=hmt&e=md/gr-planar.md),
[obj](http://rohandrape.net/?t=hmt&e=md/obj.md),
[pct](http://rohandrape.net/?t=hmt&e=md/pct.md),
[ply](http://rohandrape.net/?t=hmt&e=md/ply.md),
[scala](http://rohandrape.net/?t=hmt&e=md/scala.md)

© [rohan drape](http://rohandrape.net/), 2006-2025, [gpl](http://gnu.org/copyleft/).

* * *

```
$ doctest Music/Theory/
Examples: 1820  Tried: 1820  Errors: 0  Failures: 0
$
```
